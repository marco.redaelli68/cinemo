#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <math.h>

#include <sys/types.h>
#include <dirent.h>
#include <string.h>


#include <limits.h>       //For PATH_MAX
#include <omp.h>
#include <time.h> 
#include <pthread.h>

#ifdef W10
# include <windows.h>
#endif

#include "lame.h" // da cambiare #include "lame.h"

#define NTHREADS 100 

#define MAX_SAMPLE_NUMBER 1024 // pow(2,10)
#define MAX_PCM_SIZE  2048 // (MAX_SAMPLE_NUMBER *2)
#define MAX_MP3_SIZE  8481 //((MAX_SAMPLE_NUMBER * 1.25 + 7200) + 1) // Align to API requirement
    
int debug = 0;

typedef struct Data
{
    lame_t lame;
    char filename[PATH_MAX];
    FILE * wav_file;
    FILE * mp3_file;
    short int pcm_buffer[MAX_SAMPLE_NUMBER];
    unsigned char mp3_buffer[MAX_MP3_SIZE];
    unsigned long mp3_bytes_to_write;
    unsigned long mp3_bytes_to_read;
    int thread_running;
    //pthread_mutex_t *mutexForReading;
} Data;


int replace_with(char* filename, char* ext)
{
    char* ldot = strrchr(filename, '.');
    if (ldot != NULL)
    {
        int length_ext = strlen(ext);
        int length_filename = strlen(filename);
        memcpy(filename+length_filename-3,ext,length_ext);

    }
    return 0;
}

int ends_with(char* filename, char* extension)
{
    const char* ldot = strrchr(filename, '.');
    if (ldot != NULL)
    {
        int length = strlen(extension);
        return strncmp(ldot + 1, extension, length) == 0;
    }
    return 0;
}

void *conv(void *arg)
{
    Data * data = (Data *) arg;
    
    data->lame = lame_init();
    // REQ#9
    // lame_set_VBR(lame, vbr_default);    
    // The default (if you set nothing) is a  J-Stereo, 44.1khz
    // 128kbps CBR mp3 file at quality 5. Override various default settings 
    // as necessary, for example:
    lame_set_num_channels(data->lame,2);
    lame_set_in_samplerate(data->lame,44100);
    lame_set_brate(data->lame,128);
    lame_set_mode(data->lame,1);
    lame_set_quality(data->lame,2);   /* 2=high  5 = medium  7=low */ 
    lame_init_params(data->lame);
   
    if (debug) printf("conv:Start Conversion of %s\n", data->filename);
    
    do 
    {
        data->mp3_bytes_to_read = fread(data->pcm_buffer, 2*sizeof(short int), MAX_PCM_SIZE, data->wav_file);
        if (data->mp3_bytes_to_read == 0)
            data->mp3_bytes_to_write = lame_encode_flush(data->lame, data->mp3_buffer, MAX_MP3_SIZE);
        else
            data->mp3_bytes_to_write = lame_encode_buffer_interleaved(data->lame, data->pcm_buffer, data->mp3_bytes_to_read, data->mp3_buffer, MAX_MP3_SIZE);
        fwrite(data->mp3_buffer, data->mp3_bytes_to_write, 1, data->mp3_file);
    } while (data->mp3_bytes_to_read != 0);

    lame_close(data->lame);
    fclose(data->wav_file);
    fclose(data->mp3_file);
    if (debug) printf("conv:end Conversion of %s\n", data->filename);
    data->thread_running = 0;
    pthread_exit(NULL);
    return 0;
}

int main(int argc, char *argv[])
{
    //Timer
    double start, end;
    double runTime;
    start = omp_get_wtime();
    int rc;
    // CLI
    extern char *optarg;
	extern int optind;
	int c; 
	int jflag = 1;// at least one process to seplify coding
	//char *sname = "default_sname", *fname;
	static char usage[] = "usage: %s -j [#threads] directory_path [directory_path ...]\n"; 
    // Run aborted if no argumnets
    
    while ((c = getopt(argc, argv, "dj:")) != -1)
    {
		switch (c) 
		{
		case 'd':
			debug = 1;
			break;
		case 'j':
			if ((jflag = atoi(optarg)) <= 0) exit(1);
			break;
		case '?':
			printf("unkhown arguments to process\n");
			exit(1);
	    }
	}	
	if (optind == argc)	//need at least one argument after the command-line options 
	{
	    printf("no arguments left to process\n");
	    if(debug) printf("optind = %d, argc=%d\n", optind, argc);
	    fprintf(stderr, "%s: missing name\n", argv[0]);
            fprintf(stderr, usage, argv[0]);
	    exit(1);
	}
	
    // For Multi Therad
    pthread_t threads[jflag+1];
    Data data[jflag+1];
    for (int t = 0; t <= jflag; t++)
        data[t].thread_running = 0;
        
    // Can process more than one directory
    struct dirent *dp;
    int ith = 0;
    int nfile=0;
    for (; optind < argc; optind++) 
    {
        if (debug) printf("converting file from: \"%s\"\n", argv[optind]);  
        DIR *dir = opendir(argv[optind]);
        // Unable to open directory stream
        if (!dir)
        {
            printf("Unable to open directory stream\n"); 
            return -1; 
        }          
           
        while ((dp = readdir(dir)) != NULL)
        {
            // REQ#1: Select only files end with wav for mp3 conversion
            
            if (ends_with(dp->d_name,"wav")==1)
            {

                //To manage the relative path
                strcpy(data[ith].filename,argv[optind]);
                strcat(data[ith].filename, dp->d_name);
                data[ith].wav_file = fopen(data[ith].filename, "rb");
                //
                // REQ#5: The resulting MP3 files are to be placed within the same directory as the source WAV files, 
                // the filename extension should be changed appropriately to .MP3
                replace_with(data[ith].filename,"mp3");
                if (debug) printf("main:Open %s\n", data[ith].filename);
                data[ith].mp3_file = fopen(data[ith].filename, "wb");
                //
                data[ith].thread_running = 1;
                if (debug) printf("start threads #%d\n",ith);
                rc = pthread_create(&threads[ith], NULL, conv, (void*)(&data[ith]));
                if (rc)
                {
                    printf("Error:unable to create thread, %d\n", rc);
                    exit(-1);
                } 
                
                do
                {
                    ++ith;
                    if (ith == jflag) ith = 0;
                    //printf("after if (++ith == jflag) ith=%d, th running %d\n",ith,data[ith].thread_running);
                    if (data[ith].thread_running == 0) break;
                }while (1);
                
                nfile++;
            }
        }
        // Close directory stream
        closedir(dir);
    }
    // Wait for Thread Termination
    
    if (debug) printf("file converted #%d\n",nfile);
    if (nfile < jflag) jflag = nfile;  
    for (int t = 0; t < jflag; t++)
    {
        void *retval;
        if (debug) printf("join threads #%d\n",t);
        if(pthread_join(threads[t],(void **) &retval) != 0)
		    perror("\nThread join failed.\n");
 
    }
    end = omp_get_wtime();
    runTime = end - start;
    printf("This machine converted in %g seconds\n",runTime);
    pthread_exit(NULL);
    return 0;
}
