#include <stdio.h>
#include <math.h>

#include <sys/types.h>
#include <dirent.h>
#include <string.h>
#include <stdlib.h>

#include <limits.h>       //For PATH_MAX
#include <omp.h>
#include <time.h> 

#include <pthread.h>
#define NTHREADS 100 
#include <lame/lame.h>

#define MAX_SAMPLE_NUMBER 1024 // pow(2,10)
#define MAX_PCM_SIZE  2048 // (MAX_SAMPLE_NUMBER *2)
#define MAX_MP3_SIZE  8481 //((MAX_SAMPLE_NUMBER * 1.25 + 7200) + 1) // Align to API requirement
    
typedef struct Data{
    lame_t lame;
    char filename[PATH_MAX];
    FILE * wav_file;
    FILE * mp3_file;
    short int pcm_buffer[MAX_SAMPLE_NUMBER];
    unsigned char mp3_buffer[MAX_MP3_SIZE];
    unsigned long mp3_bytes_to_write;
    unsigned long mp3_bytes_to_read;
    //pthread_mutex_t *mutexForReading;
} Data;


int replace_with(char* filename, char* ext)
{
    char* ldot = strrchr(filename, '.');
    if (ldot != NULL)
    {
        int length_ext = strlen(ext);
        int length_filename = strlen(filename);
        memcpy(filename+length_filename-3,ext,length_ext);

    }
    return 0;
}

int ends_with(char* filename, char* extension)
{
    const char* ldot = strrchr(filename, '.');
    if (ldot != NULL)
    {
        int length = strlen(extension);
        return strncmp(ldot + 1, extension, length) == 0;
    }
    return 0;
}

void *conv(void *arg)
{
    Data * data = (Data *) arg;
    
    data->lame = lame_init();
    // REQ#9
    // lame_set_VBR(lame, vbr_default);    
    // The default (if you set nothing) is a  J-Stereo, 44.1khz
    // 128kbps CBR mp3 file at quality 5. Override various default settings 
    // as necessary, for example:
    lame_set_num_channels(data->lame,2);
    lame_set_in_samplerate(data->lame,44100);
    lame_set_brate(data->lame,128);
    lame_set_mode(data->lame,1);
    lame_set_quality(data->lame,2);   /* 2=high  5 = medium  7=low */ 
    lame_init_params(data->lame);
    printf("conv:Start Conversion of %s\n", data->filename);
    do {
        data->mp3_bytes_to_read = fread(data->pcm_buffer, 2*sizeof(short int), MAX_PCM_SIZE, data->wav_file);
        if (data->mp3_bytes_to_read == 0)
            data->mp3_bytes_to_write = lame_encode_flush(data->lame, data->mp3_buffer, MAX_MP3_SIZE);
        else
            data->mp3_bytes_to_write = lame_encode_buffer_interleaved(data->lame, data->pcm_buffer, data->mp3_bytes_to_read, data->mp3_buffer, MAX_MP3_SIZE);
        fwrite(data->mp3_buffer, data->mp3_bytes_to_write, 1, data->mp3_file);
    } while (data->mp3_bytes_to_read != 0);

    lame_close(data->lame);
    fclose(data->wav_file);
    fclose(data->mp3_file);
    printf("conv:end Conversion of %s\n", data->filename);
    pthread_exit(NULL);
}

int main(int argc, char *argv[])
{
    //Timer
    double start, end;
    double runTime;
    start = omp_get_wtime();
    int rc;
    // For Multi Therad
    pthread_t threads[NTHREADS];
    Data data[NTHREADS];
 
    // Run aborted if no argumnets
    if(argc == 1)
    {
        printf("You need at least one arguments!\n");
        return -1;
    }    
    struct dirent *dp;
    // Can process more than one directory
    int ith =0;  
    for (int i=1; i< argc; i++) 
    {
        DIR *dir = opendir(argv[i]);
        // Unable to open directory stream
        if (!dir)
        {
            printf("Unable to open directory stream\n"); 
            return -1; 
        }          
        
        while ((dp = readdir(dir)) != NULL)
        {
            // REQ#1: Select only files end with wav for mp3 conversion
            
            if (ends_with(dp->d_name,"wav")==1)
            {

                //To manage the relative path
                strcpy(data[ith].filename,argv[i]);
                strcat(data[ith].filename, dp->d_name);
                printf("main:Open %s\n", data[ith].filename);
                data[ith].wav_file = fopen(data[ith].filename, "rb");
                //
                // REQ#5: The resulting MP3 files are to be placed within the same directory as the source WAV files, 
                // the filename extension should be changed appropriately to .MP3
                replace_with(data[ith].filename,"mp3");
                printf("main::Open %s\n", data[ith].filename);
                data[ith].mp3_file = fopen(data[ith].filename, "wb");
                //
                rc = pthread_create(&threads[ith], NULL, conv, (void*)(&data[ith]));

                if (rc)
                {
                    printf("Error:unable to create thread, %d\n", rc);
                    exit(-1);
                }
                    
                ith++;
                //char ch;
	            //ch = getchar();                
            }
        }
        // Close directory stream
        closedir(dir);
    }
    // Wait for Thread Termination
    int ret;
    for (int t = 0; t < ith; t++) {
        void *retval;
        ret = pthread_join(threads[t], &retval);
        if (retval == PTHREAD_CANCELED)
            printf("The thread was canceled - ");
        else
            printf("Returned value %d - \n", (int)retval);
    }
    end = omp_get_wtime();
    runTime = end - start;
    printf("This machine converted in %g seconds\n",runTime);
    pthread_exit(NULL);
    return 0;
}
