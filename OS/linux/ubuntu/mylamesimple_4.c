#include <stdio.h>
#include <lame/lame.h>
#include <math.h>

#include <sys/types.h>
#include <dirent.h>

//
// Lists all files and sub-directories at given path.
///
void listFiles(const char *path)
{
    struct dirent *dp;
    DIR *dir = opendir(path);

    // Unable to open directory stream
    if (!dir) 
        return; 

    while ((dp = readdir(dir)) != NULL)
    {
        printf("%s\n", dp->d_name);
    }

    // Close directory stream
    closedir(dir);
}

int main(int argc, char *argv[])
{
    int read, write;
    int i = 0;
    printf("\ncmdline args count=%d", argc);

    /* First argument is executable name only */
    printf("\nexe name=%s", argv[0]);

    for (i=1; i< argc; i++) {
    printf("\narg%d=%s", i, argv[i]);
    }
    printf("\n");
    
    listFiles(argv[1]);
    
    
    
    FILE *pcm = fopen("sample4.wav", "rb");
    FILE *mp3 = fopen("sample4_1.mp3", "wb");

    // const int PCM_SIZE = 8192;
    // const int MP3_SIZE = 8192;
    // short int pcm_buffer[PCM_SIZE*2];
    // unsigned char mp3_buffer[MP3_SIZE];    
    
    const unsigned long MAX_SAMPLE_NUMBER = pow(2,10);
    const int MAX_PCM_SIZE = MAX_SAMPLE_NUMBER;
    const unsigned long MAX_MP3_SIZE = (int)(MAX_SAMPLE_NUMBER * 1.25 + 7200) + 1; // Align to API requirement
    short int pcm_buffer [MAX_PCM_SIZE * 2]; // 2 channels
    unsigned char mp3_buffer[MAX_MP3_SIZE]; // according to libmp3lame api
  

//    short int pcm_buffer[PCM_SIZE*2]; 
//    unsigned char mp3_buffer[MP3_SIZE];

    lame_t lame = lame_init();
    // REQ#9
    // lame_set_VBR(lame, vbr_default);    
    // The default (if you set nothing) is a  J-Stereo, 44.1khz
    // 128kbps CBR mp3 file at quality 5. Override various default settings 
    // as necessary, for example:
    lame_set_num_channels(lame,2);
    lame_set_in_samplerate(lame,44100);
    lame_set_brate(lame,128);
    lame_set_mode(lame,1);
    lame_set_quality(lame,2);   /* 2=high  5 = medium  7=low */ 
    lame_init_params(lame);

    do {
        read = fread(pcm_buffer, 2*sizeof(short int), MAX_PCM_SIZE, pcm);
        if (read == 0)
            write = lame_encode_flush(lame, mp3_buffer, MAX_MP3_SIZE);
        else
            write = lame_encode_buffer_interleaved(lame, pcm_buffer, read, mp3_buffer, MAX_MP3_SIZE);
        fwrite(mp3_buffer, write, 1, mp3);
    } while (read != 0);

    lame_close(lame);
    fclose(mp3);
    fclose(pcm);

    return 0;
}
