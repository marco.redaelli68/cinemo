#include <stdio.h>
#include <lame/lame.h>
#include <math.h>
int main(void)
{
    int read, write;

    FILE *pcm = fopen("sample4.wav", "rb");
    FILE *mp3 = fopen("sample4_1.mp3", "wb");

    // const int PCM_SIZE = 8192;
    // const int MP3_SIZE = 8192;
    // short int pcm_buffer[PCM_SIZE*2];
    // unsigned char mp3_buffer[MP3_SIZE];    
    
    const unsigned long MAX_SAMPLE_NUMBER = pow(2,10);
    const int MAX_PCM_SIZE = MAX_SAMPLE_NUMBER;
    const unsigned long MAX_MP3_SIZE = (int)(MAX_SAMPLE_NUMBER * 1.25 + 7200) + 1; // Align to API requirement
    short int pcm_buffer [MAX_PCM_SIZE * 2]; // 2 channels
    unsigned char mp3_buffer[MAX_MP3_SIZE]; // according to libmp3lame api
  

//    short int pcm_buffer[PCM_SIZE*2]; 
//    unsigned char mp3_buffer[MP3_SIZE];

    lame_t lame = lame_init();
    lame_set_in_samplerate(lame, 44100);
    lame_set_VBR(lame, vbr_default);
    lame_init_params(lame);

    do {
        read = fread(pcm_buffer, 2*sizeof(short int), MAX_PCM_SIZE, pcm);
        if (read == 0)
            write = lame_encode_flush(lame, mp3_buffer, MAX_MP3_SIZE);
        else
            write = lame_encode_buffer_interleaved(lame, pcm_buffer, read, mp3_buffer, MAX_MP3_SIZE);
        fwrite(mp3_buffer, write, 1, mp3);
    } while (read != 0);

    lame_close(lame);
    fclose(mp3);
    fclose(pcm);

    return 0;
}
