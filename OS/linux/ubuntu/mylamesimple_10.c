#include <stdio.h>
#include <math.h>

#include <sys/types.h>
#include <dirent.h>
#include <string.h>
#include <stdlib.h>

#include <limits.h>       //For PATH_MAX
#include <omp.h>
#include <time.h> 

#include <pthread.h>
#define NUM_THREADS 100 
#include <lame/lame.h>


char buf[NUM_THREADS][PATH_MAX + 1];

int replace_with(char* filename, char* ext)
{
    char* ldot = strrchr(filename, '.');
    if (ldot != NULL)
    {
        int length_ext = strlen(ext);
        int length_filename = strlen(filename);
        memcpy(filename+length_filename-3,ext,length_ext);

    }
    return 0;
}

int ends_with(char* filename, char* extension)
{
    const char* ldot = strrchr(filename, '.');
    if (ldot != NULL)
    {
        int length = strlen(extension);
        return strncmp(ldot + 1, extension, length) == 0;
    }
    return 0;
}

void *conv(void *filename)
{
    char* filein; 
    filein = (char*) filename;
    char fileout[PATH_MAX + 1];
    int read, write;
    const unsigned long MAX_SAMPLE_NUMBER = pow(2,10);
    const int MAX_PCM_SIZE = MAX_SAMPLE_NUMBER;
    const unsigned long MAX_MP3_SIZE = (int)(MAX_SAMPLE_NUMBER * 1.25 + 7200) + 1; // Align to API requirement
    short int pcm_buffer [MAX_PCM_SIZE * 2]; // 2 channels
    unsigned char mp3_buffer[MAX_MP3_SIZE]; // according to libmp3lame api
    
    printf("Converting from %s\n", filein);
    printf("open from %s\n", filein);
    FILE *pcm = fopen(filein, "rb");
    // REQ#5: The resulting MP3 files are to be placed within the same directory as the source WAV files, 
    // the filename extension should be changed appropriately to .MP3
    strcpy(fileout,filein);
    replace_with(fileout,"mp3");
    FILE *mp3 = fopen(fileout, "wb");
    lame_t lame = lame_init();
    // REQ#9
    // lame_set_VBR(lame, vbr_default);    
    // The default (if you set nothing) is a  J-Stereo, 44.1khz
    // 128kbps CBR mp3 file at quality 5. Override various default settings 
    // as necessary, for example:
    lame_set_num_channels(lame,2);
    lame_set_in_samplerate(lame,44100);
    lame_set_brate(lame,128);
    lame_set_mode(lame,1);
    lame_set_quality(lame,2);   /* 2=high  5 = medium  7=low */ 
    lame_init_params(lame);

    do {
        read = fread(pcm_buffer, 2*sizeof(short int), MAX_PCM_SIZE, pcm);
        if (read == 0)
            write = lame_encode_flush(lame, mp3_buffer, MAX_MP3_SIZE);
        else
            write = lame_encode_buffer_interleaved(lame, pcm_buffer, read, mp3_buffer, MAX_MP3_SIZE);
        fwrite(mp3_buffer, write, 1, mp3);
    } while (read != 0);

    lame_close(lame);
    fclose(mp3);
    fclose(pcm);
    printf("to %s\n", fileout);
    pthread_exit(NULL);
}

int main(int argc, char *argv[])
{
    pthread_t threads[NUM_THREADS];
    double start, end;
    double runTime;
    start = omp_get_wtime();
    int rc;
 
    // Run aborted if no argumnets
    if(argc == 1)
    {
        printf("You need at least one arguments!\n");
        return -1;
    }    
    struct dirent *dp;
    // Can process more than one directory
  
    for (int i=1; i< argc; i++) 
    {
        DIR *dir = opendir(argv[i]);
        // Unable to open directory stream
        if (!dir)
        {
            printf("Unable to open directory stream\n"); 
            return -1; 
        }          
        while ((dp = readdir(dir)) != NULL)
        {
            // REQ#1: Select only files end with wav for mp3 conversion
            int ith =0;
            if (ends_with(dp->d_name,"wav")==1)
            {
                //To manage the relative path
                strcpy(buf[ith],argv[i]);
                strcat(buf[ith], dp->d_name);
                printf("main:1 Converting from %s\n", buf[ith]);
                rc = pthread_create(&threads[ith], NULL, conv, (void*)buf[ith]);
                printf("main:2 Converting from %s\n", buf[ith]);
                if (rc)
                {
                    printf("Error:unable to create thread, %d\n", rc);
                    exit(-1);
                }
                ith++;
                //char ch;
	            //ch = getchar();                
            }
        }
        // Close directory stream
        closedir(dir);
        pthread_exit(NULL);
    }
    end = omp_get_wtime();
    runTime = end - start;
    printf("This machine converted in %g seconds\n",runTime);
    
    return 0;
}
