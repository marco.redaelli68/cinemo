#include <stdio.h>
#include <lame/lame.h>
#include <math.h>

#include <sys/types.h>
#include <dirent.h>
#include <string.h>
#include <stdlib.h>


const char* replace_with(char* filename, char* ext)
{
    char* ldot = strrchr(filename, '.');
    if (ldot != NULL)
    {
        int length_ext = strlen(ext);
        int length_filename = strlen(filename);
        memcpy(filename+length_filename-3,ext,length_ext);

    }
    return 0;
}


int ends_with(const char* filename, const char* extension)
{
    const char* ldot = strrchr(filename, '.');
    if (ldot != NULL)
    {
        int length = strlen(extension);
        return strncmp(ldot + 1, extension, length) == 0;
    }
    return 0;
}
//
// Lists all files and sub-directories at given path.
///
void listFiles(const char *path)
{
    struct dirent *dp;
    DIR *dir = opendir(path);

    // Unable to open directory stream
    if (!dir) 
        return; 

    while ((dp = readdir(dir)) != NULL)
    {
        if (ends_with(dp->d_name,"wav"))
            printf("%s\n", dp->d_name);
    }

    // Close directory stream
    closedir(dir);
}

void conv(FILE *pcm,FILE *mp3 )
{
    int read, write;
    const unsigned long MAX_SAMPLE_NUMBER = pow(2,10);
    const int MAX_PCM_SIZE = MAX_SAMPLE_NUMBER;
    const unsigned long MAX_MP3_SIZE = (int)(MAX_SAMPLE_NUMBER * 1.25 + 7200) + 1; // Align to API requirement
    short int pcm_buffer [MAX_PCM_SIZE * 2]; // 2 channels
    unsigned char mp3_buffer[MAX_MP3_SIZE]; // according to libmp3lame api
  
    lame_t lame = lame_init();
    // REQ#9
    // lame_set_VBR(lame, vbr_default);    
    // The default (if you set nothing) is a  J-Stereo, 44.1khz
    // 128kbps CBR mp3 file at quality 5. Override various default settings 
    // as necessary, for example:
    lame_set_num_channels(lame,2);
    lame_set_in_samplerate(lame,44100);
    lame_set_brate(lame,128);
    lame_set_mode(lame,1);
    lame_set_quality(lame,2);   /* 2=high  5 = medium  7=low */ 
    lame_init_params(lame);

    do {
        read = fread(pcm_buffer, 2*sizeof(short int), MAX_PCM_SIZE, pcm);
        if (read == 0)
            write = lame_encode_flush(lame, mp3_buffer, MAX_MP3_SIZE);
        else
            write = lame_encode_buffer_interleaved(lame, pcm_buffer, read, mp3_buffer, MAX_MP3_SIZE);
        fwrite(mp3_buffer, write, 1, mp3);
    } while (read != 0);

    lame_close(lame);

}

int main(int argc, char *argv[])
{
    
    // Run aborted if no argumnets
    if(argc == 1)
    {
        printf("You need at least one arguments!\n");
        return -1;
    }    
    struct dirent *dp;
    // Can process more than one directory
    int i = 0;
    for (i=1; i< argc; i++) 
    {
        DIR *dir = opendir(argv[i]);
        // Unable to open directory stream
        if (!dir)
        {
            printf("\nUnable to open directory stream"); 
            return -1; 
        }          
        while ((dp = readdir(dir)) != NULL)
        {
            // REQ#1: Select only files end with wav for mp3 conversion
            if (ends_with(dp->d_name,"wav")==1)
            {
                printf("Converting from %s ", dp->d_name);
                FILE *pcm = fopen(dp->d_name, "rb");
                // REQ#5: The resulting MP3 files are to be placed within the same directory as the source WAV files, 
                //        the filename extension should be changed appropriately to .MP3
                replace_with(dp->d_name,"mp3");
                FILE *mp3 = fopen(dp->d_name, "wb");
                conv(pcm,mp3);
                fclose(mp3);
                fclose(pcm);
                printf("to %s\n", dp->d_name);
            }
        }

        // Close directory stream
        closedir(dir);
    }

    return 0;
}
