[TOC]

# Task

Write a C/C++ commandline application that encodes a set of WAV files to MP3

Requirements:

(1) application is called with pathname as argument, e.g. <applicationname> F:\MyWavCollection all WAV-files contained directly in that folder are to be encoded to MP3

(2) use all available CPU cores for the encoding process in an efficient way by utilizing multi-threading

(3) statically link to lame encoder library

(4) application should be compilable and runnable on Windows and Linux

(5) the resulting MP3 files are to be placed within the same directory as the source WAV files, the filename extension should be changed appropriately to .MP3

(6) non-WAV files in the given folder shall be ignored

(7) multithreading shall be implemented in a portable way, for example using POSIX pthreads.

(8) frameworks such as Boost or Qt shall not be used

(9) the LAME encoder should be used with reasonable standard settings (e.g. quality based encoding with quality level "good")

Have a great easter weekend and I look forward to receiving your completed task.

Kind regards,

Christopher Everitt

Client Partner – Vehicle Technology

CLM Search

+44 203 929 9615 | +44 798 466 1598

ceveritt@clmsearch.com

www.clmsearch.com

# Results

The project has been stored in GitLab: https://gitlab.com/marco.redaelli68/cinemo and can be downloaded as zip file as a git repository 

This are the file stored into the root directory

```
drwxrwxr-x 7 rah rah    4096 Apr 14 08:07 ./
drwxr-xr-x 6 rah rah    4096 Apr 12 11:45 ../
drwxrwxr-x 8 rah rah    4096 Apr 14 07:52 .git/
-rwxrwxrwx 1 rah rah      69 Apr 13 09:26 L64comp.sh*
-rw-rw-r-- 1 rah rah   50053 Apr 13 07:57 lame.h
drwxrwxr-x 2 rah rah    4096 Apr 13 08:00 lib/
-rwxrwxr-x 1 rah rah 2265152 Apr 14 08:03 mylamesimple*
-rw-rw-r-- 1 rah rah    6684 Apr 14 08:06 mylamesimple_14.c
-rwxrwxr-x 1 rah rah 1104077 Apr 14 08:03 mylamesimple.exe*
drwxrwxr-x 4 rah rah    4096 Apr 13 08:29 OS/
drwxrwxr-x 2 rah rah    4096 Apr 12 15:35 README.assets/
-rw-rw-r-- 1 rah rah   24657 Apr 12 15:44 README.md
-rwxrwxrwx 1 rah rah     101 Apr 13 09:26 W64comp.sh*
drwxrwxr-x 3 rah rah    4096 Apr 13 08:27 wav/
```

The executable file  (you need to make them executable on your system before luncing) for INTEL 64bit architecture are:

```
-rwxrwxr-x 1 rah rah 2265152 Apr 14 08:03 mylamesimple*
-rwxrwxr-x 1 rah rah 1104077 Apr 14 08:03 mylamesimple.exe*
```

The source code 

```
-rw-rw-r-- 1 rah rah    6684 Apr 14 08:06 mylamesimple_14.c
```

## How to Compile

I worked mostly in Ubuntu 20.04 environment except for compiling *lame3.100* library that has been compiled bot in Ubuntu and W10.

On top of compilers you need to downloaded the lime library (this is more simple than compiling the whole Lime library) for W10 system I did copy locally the library to be linked

```
sudo apt-get install libmp3lame-dev
```

This are two script you can use to compile:

```
-rwxrwxrwx 1 rah rah      69 Apr 13 09:26 L64comp.sh
-rwxrwxrwx 1 rah rah     101 Apr 13 09:26 W64comp.sh*
```
You need to luch adding the the path to the C file:
```
./L64comp.sh ./mylamesimple_14.c
./W64comp.sh ./mylamesimple_14.c
```

## How to Run

Usage on Linux

```
~/git/cinemo$ ./mylamesimple
no arguments left to process
./mylamesimple: missing name
usage: ./mylamesimple -j [#threads] directory_path [directory_path ...]
```

Usage in W10 

Using wine under Linux

```
~/git/cinemo$ wine ./mylamesimple.exe
no arguments left to process
Z:\home\rah\git\cinemo\mylamesimple.exe: missing name
usage: Z:\home\rah\git\cinemo\mylamesimple.exe -j [#threads] directory_path [directory_path ...]
```

These are runs with different # of threads

```
rah@rah-NUC7i5BNHXF:~/git/cinemo$ ./mylamesimple -j0 ./wav/
rah@rah-NUC7i5BNHXF:~/git/cinemo$ ./mylamesimple -j1 ./wav/
This machine converted in 19.7888 seconds
rah@rah-NUC7i5BNHXF:~/git/cinemo$ ./mylamesimple -j2 ./wav/
This machine converted in 12.7002 seconds
rah@rah-NUC7i5BNHXF:~/git/cinemo$ ./mylamesimple -j3 ./wav/
This machine converted in 10.6427 seconds
rah@rah-NUC7i5BNHXF:~/git/cinemo$ ./mylamesimple -j4 ./wav/
This machine converted in 10.061 seconds
rah@rah-NUC7i5BNHXF:~/git/cinemo$ ./mylamesimple -j5 ./wav/
This machine converted in 8.89629 seconds
rah@rah-NUC7i5BNHXF:~/git/cinemo$ ./mylamesimple -j6 ./wav/
This machine converted in 8.91223 seconds 
```

this is a run with more than one directory and with debug option

```
rah@rah-NUC7i5BNHXF:~/git/cinemo$ ./mylamesimple -j10 -d ./wav/ ./wav/wav/
converting file from: "./wav/"
main:Open ./wav/sample3.mp3
start threads #0
main:Open ./wav/sample4.mp3
start threads #1
main:Open ./wav/sample2.mp3
start threads #2
main:Open ./wav/sample1.mp3
start threads #3
converting file from: "./wav/wav/"
main:Open ./wav/wav/sample7.mp3
start threads #4
main:Open ./wav/wav/sample6.mp3
start threads #5
main:Open ./wav/wav/sample5.mp3
start threads #6
file converted #7
join threads #0
conv:Start Conversion of ./wav/sample3.mp3
conv:Start Conversion of ./wav/sample4.mp3
conv:Start Conversion of ./wav/sample2.mp3
conv:Start Conversion of ./wav/sample1.mp3
conv:Start Conversion of ./wav/wav/sample5.mp3
conv:Start Conversion of ./wav/wav/sample7.mp3
conv:Start Conversion of ./wav/wav/sample6.mp3
conv:end Conversion of ./wav/wav/sample6.mp3
conv:end Conversion of ./wav/sample3.mp3
join threads #1
conv:end Conversion of ./wav/wav/sample7.mp3
conv:end Conversion of ./wav/sample1.mp3
conv:end Conversion of ./wav/sample2.mp3
conv:end Conversion of ./wav/sample4.mp3
join threads #2
join threads #3
join threads #4
join threads #5
join threads #6
conv:end Conversion of ./wav/wav/sample5.mp3
This machine converted in 14.5938 seconds

```

## Conclusion

I believe that I implemented all the requirements, however there are some area of improvements.

Although not specified on the requirements:

- the multi-threads would have been more efficient if done at chuck level instead at file level especially if files are different in size 
- a makefile is a more clean approach for multi OS/Architecture compilation. However they can be very complicated for not experts

Lesson learn: I would have been been more efficient if had had a close look at sample code located at */lame-3.100/frontend*. Althought I look at */lame-3.100/fAPI* at the very beginning of the exercise I missed this point  

# Log Book

## 02APR 

Understanding of the problem.

First condiderations:

REQ#1:  I need to develop a CLI application. The encoding of all files in the directory can be eventually manage through a bash script if I do not have time

REQ#2: I understand the requirement, however I do not have direct experience but as a user (e.g. make -j5 and FIO)

REQ#3: I did already in the past for the emmcparm CLI tool (XXXX). So I just need to reuse the make file. Never heard about LAME before, however I understand the concept of library. I hope to find the LAME library already compiled for Linux (x86) and W10(x86). This will simplify a lot. 

REQ4#: I do have experience with Linux but never compiled in any language in W in the past 20y. May be I can cross-compile against W like I usually do against different CPUs (ARM, INTEL x86 & x64, PowerPC,..)

REQ#5:  it Looks not difficult. Just manage relative path instead of absolute

REQ#6:  CLI application shall convert only WAV file and skip others eventually with a warning

REQ#7: I undestand POSIX concept, however I do not have ant programming experience. Here likely I need to ask for help if google does not

REQ#8: I believe it is about Qt is about GUI. Never heard about Boost

REQ#9: I hope there are example on how to sue LAME

I decided:

-  to use gitLAB and tracking progress using into the README.md
- use Typora as MarkDown editor
- OS: Ubuntu on Zbook and W10 on NUC or my company laptop. I need to install git on W10



## 05APR

I prepared the working environment as above described and start writing README.md

```
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys BA300B7755AFCFAEwget -qO - https://typora.io/linux/public-key.asc | sudo apt-key add -
wget -qO - https://typora.io/linux/public-key.asc | sudo apt-key add -
sudo add-apt-repository 'deb https://typora.io/linux ./'
sudo apt-get install typora
git clone https://gitlab.com/marco.redaelli68/cinemo.git
sudo apt install git
git clone https://gitlab.com/marco.redaelli68/cinemo.git
```

Now the most difficult part start :-)

### REQ#3 

I found https://lame.sourceforge.io/ 

Now I look for pre-compiled library for Linux and W: https://lame.sourceforge.io/links.php#Binaries

### LAME Binaries:

#### Windows:

- [RareWares](http://www.rarewares.org/index.php) offers several compiled LAME versions, including modified versions featuring special functionality.

### [libmp3lame 3.100](https://www.rarewares.org/mp3-lame-libraries.php#libmp3lame)

2017-10-22

The standard libmp3lame.dll providing the standardised API. Includes the exports and the library to compile against. Intel compiles.

- [x86-Win32 (336kB)](https://www.rarewares.org/files/mp3/libmp3lame-3.100x86.zip)
- [x64-Win64 (445kB)]

#### Linux:

- Ever since the MP3 patents [expired](https://lame.sourceforge.io/links.php#Patents), most Linux distros provide LAME from their online repositories or installation media. Otherwise, you can still locate installation packages at search engines such as [pkgs.org](https://pkgs.org/download/lame) or [RPM search](https://rpm.pbone.net/).

```
ah@rah-HP-ZBook-Studio-G5:~/git/cinemo$ LAME
LAME: command not found
rah@rah-HP-ZBook-Studio-G5:~/git/cinemo$ lame

Command 'lame' not found, but can be installed with:

sudo apt install lame

rah@rah-HP-ZBook-Studio-G5:~/git/cinemo$ sudo apt install lame
[sudo] password for rah: 
Reading package lists... Done
Building dependency tree       
Reading state information... Done
Suggested packages:
  lame-doc
The following NEW packages will be installed:
  lame
0 upgraded, 1 newly installed, 0 to remove and 0 not upgraded.
Need to get 48,4 kB of archives.
After this operation, 133 kB of additional disk space will be used.
Get:1 http://it.archive.ubuntu.com/ubuntu focal/universe amd64 lame amd64 3.100-3 [48,4 kB]
Fetched 48,4 kB in 3s (14,1 kB/s)
Selecting previously unselected package lame.
(Reading database ... 184169 files and directories currently installed.)
Preparing to unpack .../lame_3.100-3_amd64.deb ...
Unpacking lame (3.100-3) ...
Setting up lame (3.100-3) ...
Processing triggers for man-db (2.9.1-1) ...
```
In Linux it look like the CLI already exits. I will use it later to better understand the usage
```
rah@rah-HP-ZBook-Studio-G5:~/git/cinemo$ lame
LAME 64bits version 3.100 (http://lame.sf.net)

usage: lame [options] <infile> [outfile]

    <infile> and/or <outfile> can be "-", which means stdin/stdout.

Try:the max number of THREADs
     "lame --help"           for general usage information
 or:
     "lame --preset help"    for information on suggested predefined settings
 or:
     "lame --longhelp"
  or "lame -?"              for a complete options list

rah@rah-HP-ZBook-Studio-G5:~/git/cinemo$ lame --help
LAME 64bits version 3.100 (http://lame.sf.net)

usage: lame [options] <infile> [outfile]

    <infile> and/or <outfile> can be "-", which means stdin/stdout.

RECOMMENDED:
    lame -V2 input.wav output.mp3

OPTIONS:
    -b bitrate      set the bitrate, default 128 kbps
    -h              higher quality, but a little slower.
    -f              fast mode (lower quality)
    -V n            quality setting for VBR.  default n=4
                    0=high quality,bigger files. 9.999=smaller files
    --preset type   type must be "medium", "standard", "extreme", "insane",
                    or a value for an average desired bitrate and depending
                    on the value specified, appropriate quality settings will
                    be used.
                    "--preset help" gives more info on these

    --help id3      ID3 tagging related options

    --longhelp      full list of options

    --license       print License information

```

Now I have downloaded from the latest version from LAME rep: https://sourceforge.net/projects/lame/files/lame/

I found a lot of information in README, INSTALL and API.

I believe that I need to follow API for the scope of this exercise. However I googled for an example and I find some good information here: https://stackoverflow.com/questions/2495420/is-there-any-lame-c-wrapper-simplifier-working-on-linux-mac-and-win-from-pure 

So I decided to download only the library:

```
sudo apt-get install libmp3lame-dev
```

- Unfortunately I found an example in C++ that required some adaptation to work. See cinemo/LameRepository/linux/UsingUbuntuLibrary/mylame.c.

```
gcc -Wall mylame.c -lmp3lame -o mylame -lpthread
mylame.c: In function ‘main’:
mylame.c:56:14: warning: unused variable ‘write’ [-Wunused-variable]
   56 |     int read,write;
      |              ^~~~~
mylame.c:56:9: warning: unused variable ‘read’ [-Wunused-variable]
   56 |     int read,write;
      |         ^~~~
```

- I have also find a much simple program that I can compile too w/o error

```
$ gcc -Wall mylamesimple.c -lmp3lame -o mylamesimple
```

- Now I do have two programs that compile correctly but they do not convert. I need to combine in one working program

- I have discovered that the Parody-01.wav, Ensoniq-ZR-76-01-Dope-77.wav & Answer-02.wav do not convert also using the linux utility!!!!

```
rah@rah-HP-ZBook-Studio-G5:~/git/cinemo/LameRepository/linux/UsingUbuntuLibrary$ ll *.wav
-rw-rw-r-- 1 rah rah     68290 apr  5 18:06  Answer-02.wav
-rw-rw-r-- 1 rah rah    548512 apr  5 20:21  Ensoniq-ZR-76-01-Dope-77.wav
-rw-rw-r-- 1 rah rah    128250 apr  5 18:07  Parody-01.wav
-rw-rw-r-- 1 rah rah 128434392 apr  5 21:08  sample0.wav
-rw-rw-r-- 1 rah rah  21537382 apr  5 21:10  sample1.wav
-rw-rw-r-- 1 rah rah  18658426 apr  5 21:08  sample3.wav
-rw-rw-r-- 1 rah rah  43123318 apr  5 21:07  sample4.wav
-rw-rw-r-- 1 rah rah 128434392 apr  5 21:08 'SymphonyNo.6_1st movement.wav'
-rw-r--r-- 1 rah rah    100044 set 10  2000  testcase.wav
```

- I will focus on the following file for my test. I have checked that *mylamesimple.c* works for these wav files while *mylame.c* still does not convert properly files and also generate error due to the multi-thread

```
rah@rah-HP-ZBook-Studio-G5:~/git/cinemo/LameRepository/linux/UsingUbuntuLibrary$ ll *.wav
-rw-rw-r-- 1 rah rah 128434392 apr  5 21:08  sample0.wav
-rw-rw-r-- 1 rah rah  21537382 apr  5 21:10  sample1.wav
-rw-rw-r-- 1 rah rah  18658426 apr  5 21:08  sample3.wav
-rw-rw-r-- 1 rah rah  43123318 apr  5 21:07  sample4.wav
-rw-rw-r-- 1 rah rah 128434392 apr  5 21:08 'SymphonyNo.6_1st movement.wav'
-rw-r--r-- 1 rah rah    100044 set 10  2000  testcase.wav
```

- REQ#3: Static compile in Linux

  ```
  $ gcc -Wall -static  mylamesimple.c -lmp3lame -o mylamesimple -lm
  $ ldd mylamesimple
  	not a dynamic executable
  ```

- In *mylamesimple_3.c* I added the REQ#9 to allow quality output configuration

- In *mylamesimple_6c*: // REQ#5: The resulting MP3 files are to be placed within the same directory as the source WAV files, the filename extension should be changed appropriately to .MP3

- In *mylamesimple_7.c*: the relative paths are now properly managed in Linux at least

```
$ ./mylamesimple ./wav/ ./
Converting from ./wav/sample1.wav to ./wav/sample1.mp3
Converting from ./wav/Symphony No.6 (1st movement).wav to ./wav/Symphony No.6 (1st movement).mp3
Converting from ./wav/sample4.wav to ./wav/sample4.mp3
Converting from ./wav/sample3.wav to ./wav/sample3.mp3
Converting from ./wav/sample2.wav to ./wav/sample2.mp3
Converting from ./sample1.wav to ./sample1.mp3
Converting from ./Symphony No.6 (1st movement).wav to ./Symphony No.6 (1st movement).mp3
Converting from ./sample4.wav to ./sample4.mp3
Converting from ./sample3.wav to ./sample3.mp3
Converting from ./sample2.wav to ./sample2.mp3
```

* Try to use Multiple core but without generating multiple thread I do not see any difference

```
$ gcc -Wall -static  mylamesimple_7.c -lmp3lame -o mylamesimple -lm
$ ./mylamesimple ./wav/ ./
Converting from ./wav/sample1.wav to ./wav/sample1.mp3
Converting from ./wav/sample4.wav to ./wav/sample4.mp3
Converting from ./wav/sample3.wav to ./wav/sample3.mp3
Converting from ./wav/sample2.wav to ./wav/sample2.mp3
Converting from ./sample1.wav to ./sample1.mp3
Converting from ./sample4.wav to ./sample4.mp3
Converting from ./sample3.wav to ./sample3.mp3
Converting from ./sample2.wav to ./sample2.mp3
This machine converted in 32.5443 seconds
$ gcc -Wall -static  mylamesimple_8.c -lmp3lame -o mylamesimple -lm -lgomp
$ ./mylamesimple ./wav/ ./
Converting from ./wav/sample1.wav to ./wav/sample1.mp3
Converting from ./wav/sample4.wav to ./wav/sample4.mp3
Converting from ./wav/sample3.wav to ./wav/sample3.mp3
Converting from ./wav/sample2.wav to ./wav/sample2.mp3
Converting from ./sample1.wav to ./sample1.mp3
Converting from ./sample4.wav to ./sample4.mp3
Converting from ./sample3.wav to ./sample3.mp3
Converting from ./sample2.wav to ./sample2.mp3
This machine converted in 33.1398 seconds
```

- I found a tutorial: https://hpc-tutorials.llnl.gov/posix/.
  Working on multi-thread, still debugging
  
  ```
  gcc -Wall -static  mylamesimple_10.c -lmp3lame -o mylamesimple -lm -lgomp -lpthread
  ```
  
  
  
- mylamesimple_10.c does not work since the multiple threads overwrite the same variablesmain:1 Converting from ./sample1.wav
  
  ```
  main:2 Converting from ./sample1.wav
  main:1 Converting from ./sample4.wav
  Converting from ./sample4.wav
  open from ./sample4.wav
  main:2 Converting from ./sample4.wav
  main:1 Converting from ./sample3.wav
  Converting from ./sample3.wav
  open from ./sample3.wav
  main:2 Converting from ./sample3.wav
  Converting from ./sample3.wav
  open from ./sample3.wav
  to ./sample3.mp3
  to ./sample3.mp3
  to ./sample3.mp3
  ```
  
  

## 9APR

- Cross compile in Linux for W10: https://arrayfire.com/cross-compile-to-windows-from-linux/

```
sudo apt-get install mingw-w64

\# C

i686-w64-mingw32-gcc hello.c -o hello32.exe   # 32-bit

x86_64-w64-mingw32-gcc hello.c -o hello64.exe  # 64-bit

\# C++

i686-w64-mingw32-g++ hello.cc -o hello32.exe   # 32-bit

x86_64-w64-mingw32-g++ hello.cc -o hello64.exe  # 64-bit

x86_64-w64-mingw32-gcc -Wall -static mylamesimple_10.c -lmp3lame -o mylamesimple -lm -lgomp -lpthread
```

## 10APR

- I gave up on W10 since I did not find to include the library (in dll format) and decided to focus on solving the issue with multithreads

- mylanesimple_11.c perform multithread conversioncorrectly one thread per file. However, this is the most optimized solution

## 11APR

- mylanesimple_12.c: improved CLI to input the max number of THREADs
- mylanesimple_13.c: Put a control in order not exceed the the max number of THREADs. Option -j debugged, code need to be clear from debug printf"



```
gcc -Wall -static  mylamesimple_12.c -lmp3lame -o mylamesimple -lm -lgomp -lpthread
```



```
rah@rah-HP-ZBook-Studio-G5:~/git/cinemo/LameRepository/linux/UsingUbuntuLibrary$ ./mylamesimple -j1 ./ ./wav/
argument: "./"
main:Open ./sample1.wav
main:Open ./sample1.mp3
start threads #0
conv:Start Conversion of ./sample1.mp3
conv:end Conversion of ./sample1.mp3
main:Open ./sample4.wav
main:Open ./sample4.mp3
start threads #0
conv:Start Conversion of ./sample4.mp3
conv:end Conversion of ./sample4.mp3
main:Open ./sample3.wav
main:Open ./sample3.mp3
start threads #0
conv:Start Conversion of ./sample3.mp3
conv:end Conversion of ./sample3.mp3
argument: "./wav/"
main:Open ./wav/sample1.wav
main:Open ./wav/sample1.mp3
start threads #0
conv:Start Conversion of ./wav/sample1.mp3
conv:end Conversion of ./wav/sample1.mp3
main:Open ./wav/sample4.wav
main:Open ./wav/sample4.mp3
start threads #0
conv:Start Conversion of ./wav/sample4.mp3
conv:end Conversion of ./wav/sample4.mp3
main:Open ./wav/sample3.wav
main:Open ./wav/sample3.mp3
start threads #0
conv:Start Conversion of ./wav/sample3.mp3
conv:end Conversion of ./wav/sample3.mp3
main:Open ./wav/sample2.wav
main:Open ./wav/sample2.mp3
start threads #0
conv:Start Conversion of ./wav/sample2.mp3
conv:end Conversion of ./wav/sample2.mp3
This machine converted in 33.7863 seconds
rah@rah-HP-ZBook-Studio-G5:~/git/cinemo/LameRepository/linux/UsingUbuntuLibrary$ ./mylamesimple -j2 ./ ./wav/
argument: "./"
main:Open ./sample1.wav
main:Open ./sample1.mp3
start threads #0
main:Open ./sample4.wav
main:Open ./sample4.mp3
start threads #1
conv:Start Conversion of ./sample1.mp3
conv:Start Conversion of ./sample4.mp3
conv:end Conversion of ./sample1.mp3
main:Open ./sample3.wav
main:Open ./sample3.mp3
start threads #0
conv:Start Conversion of ./sample3.mp3
conv:end Conversion of ./sample3.mp3
argument: "./wav/"
main:Open ./wav/sample1.wav
main:Open ./wav/sample1.mp3
start threads #0
conv:Start Conversion of ./wav/sample1.mp3
conv:end Conversion of ./sample4.mp3
main:Open ./wav/sample4.wav
main:Open ./wav/sample4.mp3
start threads #1
conv:Start Conversion of ./wav/sample4.mp3
conv:end Conversion of ./wav/sample1.mp3
main:Open ./wav/sample3.wav
main:Open ./wav/sample3.mp3
start threads #0
conv:Start Conversion of ./wav/sample3.mp3
conv:end Conversion of ./wav/sample3.mp3
main:Open ./wav/sample2.wav
main:Open ./wav/sample2.mp3
start threads #0
conv:Start Conversion of ./wav/sample2.mp3
conv:end Conversion of ./wav/sample4.mp3
join threads #0
conv:end Conversion of ./wav/sample2.mp3
This machine converted in 21.4029 seconds
rah@rah-HP-ZBook-Studio-G5:~/git/cinemo/LameRepository/linux/UsingUbuntuLibrary$ ./mylamesimple -j3 ./ ./wav/
argument: "./"
main:Open ./sample1.wav
main:Open ./sample1.mp3
start threads #0
main:Open ./sample4.wav
main:Open ./sample4.mp3
start threads #1
main:Open ./sample3.wav
main:Open ./sample3.mp3
start threads #2
conv:Start Conversion of ./sample1.mp3
conv:Start Conversion of ./sample4.mp3
conv:Start Conversion of ./sample3.mp3
conv:end Conversion of ./sample3.mp3
argument: "./wav/"
main:Open ./wav/sample1.wav
main:Open ./wav/sample1.mp3
start threads #2
conv:Start Conversion of ./wav/sample1.mp3
conv:end Conversion of ./sample1.mp3
main:Open ./wav/sample4.wav
main:Open ./wav/sample4.mp3
start threads #0
conv:Start Conversion of ./wav/sample4.mp3
conv:end Conversion of ./wav/sample1.mp3
main:Open ./wav/sample3.wav
main:Open ./wav/sample3.mp3
start threads #2
conv:Start Conversion of ./wav/sample3.mp3
conv:end Conversion of ./sample4.mp3
main:Open ./wav/sample2.wav
main:Open ./wav/sample2.mp3
start threads #1
conv:Start Conversion of ./wav/sample2.mp3
conv:end Conversion of ./wav/sample3.mp3
join threads #0
conv:end Conversion of ./wav/sample4.mp3
join threads #1
conv:end Conversion of ./wav/sample2.mp3
This machine converted in 16.21 seconds
rah@rah-HP-ZBook-Studio-G5:~/git/cinemo/LameRepository/linux/UsingUbuntuLibrary$ ./mylamesimple -j4 ./ ./wav/
argument: "./"
main:Open ./sample1.wav
main:Open ./sample1.mp3
start threads #0
main:Open ./sample4.wav
main:Open ./sample4.mp3
start threads #1
main:Open ./sample3.wav
main:Open ./sample3.mp3
start threads #2
argument: "./wav/"
main:Open ./wav/sample1.wav
main:Open ./wav/sample1.mp3
start threads #3
conv:Start Conversion of ./sample1.mp3
conv:Start Conversion of ./sample4.mp3
conv:Start Conversion of ./sample3.mp3
conv:Start Conversion of ./wav/sample1.mp3
conv:end Conversion of ./sample3.mp3
main:Open ./wav/sample4.wav
main:Open ./wav/sample4.mp3
start threads #2
conv:Start Conversion of ./wav/sample4.mp3
conv:end Conversion of ./sample1.mp3
main:Open ./wav/sample3.wav
main:Open ./wav/sample3.mp3
start threads #0
conv:Start Conversion of ./wav/sample3.mp3
conv:end Conversion of ./wav/sample1.mp3
main:Open ./wav/sample2.wav
main:Open ./wav/sample2.mp3
start threads #3
conv:Start Conversion of ./wav/sample2.mp3
conv:end Conversion of ./wav/sample3.mp3
join threads #0
join threads #1
conv:end Conversion of ./sample4.mp3
join threads #2
conv:end Conversion of ./wav/sample2.mp3
conv:end Conversion of ./wav/sample4.mp3
This machine converted in 13.5234 seconds
rah@rah-HP-ZBook-Studio-G5:~/git/cinemo/LameRepository/linux/UsingUbuntuLibrary$ ./mylamesimple -j5 ./ ./wav/
argument: "./"
main:Open ./sample1.wav
main:Open ./sample1.mp3
start threads #0
main:Open ./sample4.wav
main:Open ./sample4.mp3
start threads #1
main:Open ./sample3.wav
main:Open ./sample3.mp3
start threads #2
argument: "./wav/"
main:Open ./wav/sample1.wav
main:Open ./wav/sample1.mp3
start threads #3
main:Open ./wav/sample4.wav
main:Open ./wav/sample4.mp3
conv:Start Conversion of ./sample1.mp3
start threads #4
conv:Start Conversion of ./sample4.mp3
conv:Start Conversion of ./sample3.mp3
conv:Start Conversion of ./wav/sample1.mp3
conv:Start Conversion of ./wav/sample4.mp3
conv:end Conversion of ./sample3.mp3
main:Open ./wav/sample3.wav
main:Open ./wav/sample3.mp3
start threads #2
conv:Start Conversion of ./wav/sample3.mp3
conv:end Conversion of ./wav/sample1.mp3
main:Open ./wav/sample2.wav
main:Open ./wav/sample2.mp3
start threads #3
conv:end Conversion of ./sample1.mp3
join threads #0
join threads #1
conv:Start Conversion of ./wav/sample2.mp3
conv:end Conversion of ./wav/sample3.mp3
conv:end Conversion of ./wav/sample4.mp3
conv:end Conversion of ./sample4.mp3
join threads #2
join threads #3
conv:end Conversion of ./wav/sample2.mp3
This machine converted in 12.1797 seconds
```

## 12APR

- Compiled the /lame-3.100 under Linux w/o major issue. Run the following commands:

  ```
  % ./configure
  % make
  ```

- Compiled under W10 with some issues been solved

  the MinGW shell is located at this path: "C:\MinGW\msys\1.0\msys.bat"
  

  This procedure works, after trial and error from following this [guide](http://kemovitra.blogspot.com/2009/08/mingw-to-compile-lame-for-windows.html).Refined some steps.
  

**Get MinGW** (if you didn't have)
  First and foremost, have MinGW running on your computer, by downloading the installer from [here](http://sourceforge.net/projects/mingw/files/Installer/mingw-get-inst/).

  

  [![img](README.assets/1.png)](https://4.bp.blogspot.com/-ogKaObeOJpE/UaTXLP2VUJI/AAAAAAAAAaw/4Y5Y-dadVvM/s1600/1.png)

  

  Choose to download the latest repository catalogue, and choose to install the following components:
  \- C compilier
  \- C++ compiler
\- MSYS Basic System
  \- MinGW Developer Toolkit

  After installation, open a MinGW shell by Start>All Programs>MinGW>MinGW Shell then type

  > mingw-get install gcc

   then

  > mingw-get install mingw-utils

  Test if your MinGW is working by typing.

  > gcc -version

  Something like this should appear.

  

  [![img](README.assets/2.png)](https://1.bp.blogspot.com/-ygGBv_6ZlHs/UaTXMJHMBnI/AAAAAAAAAa4/u933dI2-rWI/s1600/2.png)


  Then, install the Yasm assembler for optimal compilation of LAME by going to [here](http://yasm.tortall.net/Download.html) (Win32.exe) and putting the .exe to the bin folder of MinGW.

   **Get source of LAME**
By going [here](http://sourceforge.net/projects/lame/files/lame/3.99/).
  Extract the tarball using 7-zip or other decompression software and place it somewhere.

 **Compile LAME**
  At the MinGW shell, change working directory to the lame folder you extracted by typing

  > cd C:/Users/username/lame-3.99.5

  Note the slash.

  Then, type this

  > ./configure --prefix=/mingw --enable-export=full

   Then to start compile type

  > make

Got a failure 

```
parse.c:74:22: fatal error: langinfo.h: No such file or directory
\#include <langinfo.h>
```

Been solved following instruction here: http://blog.k-tai-douga.com/article/35965219.html

```
$ patch -p1 < lame-3.100-parse_c.diff
$ CPPFLAGS=-msse ./configure --prefix=/mingw --disable-shared --enable-nasm
$ make
```

- Repository has been restructured

  ```
  C:.
  └───Git
      └───cinemo
          ├───LameRepository
          │   ├───linux
          │   │   ├───convimp3
          │   │   ├───lame-3.100
          │   │   └───UsingUbuntuLibrary
          │   │       └───wav
          │   └───Windows
          │       ├───lame-3.100
          │       │   
          │       └───xCOMP
          │           └───lame
          └───README.assets
  ```


- I did not solved the issue to recompile under windows, however the cross compilation under Linux works!rah@rah-

  ```
  NUC7i5BNHXF:~/git/cinemo/LameRepository/Windows/W10_NUC$ i686-w64-mingw32-gcc -Wall -static  mylamesimple_13.c libmp3lame.a -o mylamesimple -lm -lgomp -lpthread
  mylamesimple_13.c: In function ‘main’:
  mylamesimple_13.c:113:9: warning: variable ‘err’ set but not used [-Wunused-but-set-variable]
    113 |  int c, err = 0;
        |         ^~~               
  rah@rah-NUC7i5BNHXF:~/git/cinemo/LameRepository/Windows/W10_NUC$ wine ./mylamesimple -d -j10 ../../linux/Ubuntu/
  argument: "../../linux/Ubuntu/"
  main:Open ../../linux/Ubuntu/sample1.wav
  main:Open ../../linux/Ubuntu/sample1.mp3
  start threads #0
  main:Open ../../linux/Ubuntu/sample3.wav
  main:Open ../../linux/Ubuntu/sample3.mp3
  start threads #1
  main:Open ../../linux/Ubuntu/sample4.wav
  main:Open ../../linux/Ubuntu/sample4.mp3
  start threads #2
  join threads #0
  conv:Start Conversion of ../../linux/Ubuntu/sample3.mp3
  conv:Start Conversion of ../../linux/Ubuntu/sample1.mp3
  conv:Start Conversion of ../../linux/Ubuntu/sample4.mp3
  conv:end Conversion of ../../linux/Ubuntu/sample3.mp3
  conv:end Conversion of ../../linux/Ubuntu/sample1.mp3
  join threads #1
  join threads #2
  conv:end Conversion of ../../linux/Ubuntu/sample4.mp3
  This machine converted in 24.802 seconds
  rah@rah-NUC7i5BNHXF:~/git/cinemo/LameRepository/Windows/W10_NUC$ 
  ```

  

## 13APR
- Changed the directory and create the scripts file to compile. Make file are more complicated in principle and I do not have a direct experience

## 14APR

- Final version for release